import unittest

class Test_FirstCase(unittest.TestCase):

    def setUp(self):
        print("<------ This method is run once before every test ------>")

    def test_firstMethod(self):
        print(" Running First Test Method ")

    def test_secondMethod(self):
        print(" Running Second Test Method ")

    def tearDown(self):
        print("<------ This method is run once after every test ------>")


if __name__ == '__main__':
    unittest.main(verbosity=2)