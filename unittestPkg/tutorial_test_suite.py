# Step 1
import unittest

# Step 2, Import test classes
from unittestPkg.test_firstCase import Test_FirstCase
from unittestPkg.test_secondCase import Test_SecondCase
from unittestPkg.test_Assertions import Test_Assertions

# Step 3 Get tests from test classes
firstTest = unittest.TestLoader().loadTestsFromTestCase(Test_FirstCase)
secondTest = unittest.TestLoader().loadTestsFromTestCase(Test_SecondCase)
assertTest = unittest.TestLoader().loadTestsFromTestCase(Test_Assertions)

# Step 4, Create Test Suite combining tests from step 3.
smoke_test = unittest.TestSuite([firstTest, secondTest, assertTest])

# Step 5, Run test suite.
unittest.TextTestRunner(verbosity=2).run(smoke_test)