import unittest

class Test_SecondCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("*" * 45)
        print("Startup - Creating DB connection...")
        print("*" * 45)

    def setUp(self):
        print("<------ This method is run once before every test ------>")

    def test_firstMethod(self):
        print(" Running First Test Method ")

    def test_secondMethod(self):
        print(" Running Second Test Method ")

    def tearDown(self):
        print("<------ This method is run once after every test ------>")

    @classmethod
    def tearDownClass(cls):
        print("*" * 45)
        print("Finishing - Closing DB connection...")
        print("*" * 45)

if __name__ == '__main__':
    unittest.main(verbosity=2)