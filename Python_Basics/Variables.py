# Declare a variable and initialize it
name = 'Python'
print(name)

# Re-declaring the variable works
name = 'Tutorial'
print(name)