#This is a single line comment in Python

print("Python Tutorial, 1st Line") #This is a single comment in Python

""" 
For multi-line
comment use three
double quotes
"""

print("Python Tutorial, 2nd Line") #This is a single comment in Python